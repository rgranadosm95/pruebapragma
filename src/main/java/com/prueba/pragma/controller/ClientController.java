package com.prueba.pragma.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.pragma.model.Cliente;
import com.prueba.pragma.model.Imagen;
import com.prueba.pragma.service.ClienteSvc;
import com.prueba.pragma.service.ImagenSvc;

@RestController
@RequestMapping("/pragma")
public class ClientController {
	
	@Autowired
	private ClienteSvc clienteSvc;
	@Autowired
	private ImagenSvc imagenSvc;

	@CrossOrigin(origins = "http://localhost:9001")
	@GetMapping("/clients")
	public List<Cliente> getAllClients(){
		return clienteSvc.ListClients();
	}
	
	@CrossOrigin(origins = "http://localhost:9090")
	@GetMapping("/getClientForId/{tipoId}/{numId}")
	public Cliente getClientForId(@PathVariable(value="tipoId") String tipoId, @PathVariable(value="numId") String numId ){
		return clienteSvc.findByTipoIdAndNumId(tipoId, numId);
	}
	
	@CrossOrigin(origins = "http://localhost:9090")
	@GetMapping("/getClientForAge/{edad}")
	public List<Cliente> getClientByAge(@PathVariable(value="edad") int edad){
		return clienteSvc.getClientByAge(edad);
	}
	
	@CrossOrigin(origins = "http://localhost:9090")
	@PostMapping("/addClient")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<String> addClient(@RequestBody Cliente cliente) {
		clienteSvc.saveClient(cliente);
		return new ResponseEntity<String>("Cliente agregado", HttpStatus.CREATED);
	}
	
	@CrossOrigin(origins = "http://localhost:9090")
	@PutMapping("/updateClient/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<String> updateClient(@RequestBody Cliente cliente, @PathVariable(value = "id") int id) {
		clienteSvc.updateClient(cliente,id);
		return new ResponseEntity<String>("Cliente actualizado", HttpStatus.CREATED);
	
	}
	
	@CrossOrigin(origins = "http://localhost:9090")
	@DeleteMapping("/deleteClient/{id}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<String>  deleteClient(@PathVariable(value="id") int id) {
		clienteSvc.deleteClient(id);
		return new ResponseEntity<String>("Cliente eliminado", HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "http://localhost:9090")
	@GetMapping("/image")
	public List <Imagen> getImage() {
		return (List<Imagen>) imagenSvc.getImages();
	}
	
	@CrossOrigin(origins = "http://localhost:9090")
	@GetMapping("/getImageNumId/{numId}")
	public Imagen getImageByNumId(@PathVariable(value="numId") String numId){
		return imagenSvc.getImageByNumId(numId);
	}
	
	@CrossOrigin(origins = "http://localhost:9090")
	@PostMapping("/addImage")
	public void addImage(@RequestBody Imagen image) {
		 imagenSvc.saveImage(image);
	}
	
	@CrossOrigin(origins = "http://localhost:9090")
	@PutMapping("/updateImage/{id}")
	public void updateImage(@RequestBody Imagen image, @PathVariable(value = "id") int id) {
		imagenSvc.updateImage(image, id);
	}
	
	@CrossOrigin(origins = "http://localhost:9090")
	@DeleteMapping("/deleteImage/{id}")
	public void deleteImage(@PathVariable(value="id") int id) {
		imagenSvc.deleteImage(id);
	}
}
