package com.prueba.pragma.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;



@Entity
@Table(name ="clientes")
public class Cliente {
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)			
	private int id;
	
	@Column
	private String nombres;
	
	@Column
	private String apellidos;
	
	@Column(name="tipo_id")
	private String tipoId;
	
	@Column(name="num_id")
	private String numId;
	
	@Column
	private int edad;
	
	@Column(name = "ciudad_nac")
	private String ciudad;
	
	@Transient
	private String photo;


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getTipoId() {
		return tipoId;
	}

	public void setTipoId(String tipoId) {
		this.tipoId = tipoId;
	}

	public String getNumId() {
		return numId;
	}

	public void setNumId(String numId) {
		this.numId = numId;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	
	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}
	
	
	public Cliente() {
	
	}

	public Cliente(int id, String nombres, String apellidos, String tipoId, String numId, int edad, String ciudad) {
		this.id = id;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.tipoId = tipoId;
		this.numId = numId;
		this.edad = edad;
		this.ciudad = ciudad;
	}
	
	
	
	

}