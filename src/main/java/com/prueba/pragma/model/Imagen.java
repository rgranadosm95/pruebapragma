package com.prueba.pragma.model;


import javax.persistence.Id;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "photo")
public class Imagen {
	
	@Id
	private int id;
	
	private String numId;
	
	private String photo;
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}
	
	public String getNumId() {
		return numId;
	}

	public void setNumId(String numId) {
		this.numId = numId;
	}

	public Imagen() {
	
	}

	public Imagen(String photo, int id, String numId) {
		this.photo = photo;
		this.id = id;
		this.numId = numId;
	
	}

	
	
	
}
