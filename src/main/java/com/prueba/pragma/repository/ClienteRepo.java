package com.prueba.pragma.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.prueba.pragma.model.Cliente;

public interface ClienteRepo extends JpaRepository<Cliente, Integer> {
	
	List <Cliente> findByEdadGreaterThanEqual(int edad);
	Cliente findByTipoIdAndNumId(String tipoId, String numId);
	Cliente findByNumId(String numId);

}
