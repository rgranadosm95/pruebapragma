package com.prueba.pragma.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.prueba.pragma.model.Imagen;

public interface ImagenRepo extends MongoRepository<Imagen, Integer> {
	
	public Imagen findByNumId(String numId);

}
