package com.prueba.pragma.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.prueba.pragma.model.Cliente;
import com.prueba.pragma.model.Imagen;
import com.prueba.pragma.repository.ClienteRepo;
import com.prueba.pragma.repository.ImagenRepo;

@Service
@Transactional
public class ClienteSvc {

	@Autowired
	private ClienteRepo clienteRepo;

	@Autowired
	private ImagenSvc imagenSvc;

	public List<Cliente> ListClients() {
		List<Cliente> clientes = new ArrayList<Cliente>();
		for (Cliente cliente : clienteRepo.findAll()) {
			for (Imagen imagen : imagenSvc.getImages()) {
				if (cliente.getId() == (imagen.getId())) {
					cliente.setPhoto(imagen.getPhoto());
					clientes.add(cliente);
				}
			}
		}
		return clientes;
	}

	public Cliente saveClient(Cliente cliente) {
		Imagen imagen = new Imagen();	
		clienteRepo.save(cliente);
		imagen.setId(cliente.getId());
		imagen.setPhoto(cliente.getPhoto());
		imagen.setNumId(cliente.getNumId());
		imagenSvc.saveImage(imagen);		
		return cliente;

	}

	public Cliente findByTipoIdAndNumId(String tipoId, String numId) {
		Cliente cliente = new Cliente();
		Imagen imagen = new Imagen();
		cliente = clienteRepo.findByTipoIdAndNumId(tipoId, numId);
		imagen = imagenSvc.getImageByNumId(cliente.getNumId());
		cliente.setPhoto(imagen.getPhoto());
		return cliente;
	}

	public List<Cliente> getClientByAge(int edad) {

		List<Cliente> clientes = new ArrayList<Cliente>();
		for (Cliente cliente : clienteRepo.findByEdadGreaterThanEqual(edad)) {
			for (Imagen imagen : imagenSvc.getImages()) {
				if (cliente.getId() == (imagen.getId())) {
					cliente.setPhoto(imagen.getPhoto());
					clientes.add(cliente);
				}
			}
		
	}	return clientes;

	}

	public void updateClient(Cliente cliente, int id) {
		Imagen imagen = new Imagen();
		clienteRepo.findById(id).ifPresent((x) -> {
			cliente.setId(id);
			imagen.setNumId(cliente.getNumId());
			imagen.setPhoto(cliente.getPhoto());
			imagenSvc.updateImage(imagen, id);
			clienteRepo.save(cliente);

		});

	}

	public String deleteClient(int id) {

		clienteRepo.deleteById(id);
		imagenSvc.deleteImage(id);
		return "Cliente eliminado";

	}
}
