package com.prueba.pragma.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.prueba.pragma.model.Imagen;
import com.prueba.pragma.repository.ImagenRepo;

@Service
@Transactional
public class ImagenSvc {
	
	 @Autowired
	 private ImagenRepo imagenRepo;
	 
		public List<Imagen>getImages(){
			return imagenRepo.findAll();
		}
		public Imagen saveImage(Imagen imagen){
			return imagenRepo.save(imagen);
		}
		
		public Optional<Imagen> getImageById(int id) {
			return imagenRepo.findById(id);
		}
		
		public Imagen getImageByNumId(String numId) {
			
			return imagenRepo.findByNumId(numId);
		}
		
		public void updateImage(Imagen imagen, int id) {
			imagenRepo.findById(id).ifPresent((x) -> {
				imagen.setId(id);
				imagenRepo.save(imagen);
				
			});
			
		}
		
		public void deleteImage(int id) {
			imagenRepo.deleteById(id);
			
		}
		

}
