package com.prueba.pragma.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
 
import static springfox.documentation.builders.PathSelectors.regex;
@Configuration
@EnableSwagger2
public class SwaggerConfiguration {
     
     @Bean
        public Docket productApi() {
            return new Docket(DocumentationType.SWAGGER_2)
            	.apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.prueba.pragma.controller"))
                .paths(paths())
                 .build();
        }
     
     private ApiInfo apiInfo() {
         return new ApiInfoBuilder()
         .title("Prueba Backend Pragma Rest APIs")
         .description("Listado de todas las apis rest para la prueba backend de Pragma.")
         .version("1.0-SNAPSHOT")
         .build();
     }
     
     private Predicate<String> paths() {
 
         return Predicates.and(
         PathSelectors.regex("/pragma.*"),
         Predicates.not(PathSelectors.regex("/error.*")));
         }
     }
                 
 
