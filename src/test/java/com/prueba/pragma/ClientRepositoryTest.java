package com.prueba.pragma;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.prueba.pragma.controller.ClientController;
import com.prueba.pragma.model.Cliente;
import com.prueba.pragma.service.ClienteSvc;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class ClientRepositoryTest {
	
	@InjectMocks
	ClientController controller;
	
	@Mock
	ClienteSvc clienteSvc;
	
	@Mock
	Cliente cliente;
	
	private static final int ID = 1;
	private static final String TIPO_ID = "CC";
	private static final String NUM_ID = "222222";
	private static final int EDAD = 30;
	
	@Test
	public void getAllClientsTest() {
		List<Cliente> clientes = new ArrayList<>();
		clientes.add(cliente);
		when(clienteSvc.ListClients()).thenReturn(clientes);
		Assert.assertEquals(controller.getAllClients(), clientes);
	}
	
	@Test
	public void getClientForIdTest() {
		when(clienteSvc.findByTipoIdAndNumId(TIPO_ID, NUM_ID)).thenReturn(cliente);
		Assert.assertEquals(controller.getClientForId(TIPO_ID, NUM_ID).getNombres(), cliente.getNombres());
	}
	
	@Test
	public void getClientByAgeTest() {
		List<Cliente> clientes = new ArrayList<>();
		clientes.add(cliente);
		when(clienteSvc.getClientByAge(EDAD)).thenReturn(clientes);
		Assert.assertEquals(controller.getClientByAge(EDAD), clientes);
	}
	
	@Test
	public void addClientTest() {
		Assert.assertEquals(controller.addClient(cliente).getStatusCode(), HttpStatus.CREATED);
	}
	
	
	@Test
	public void updateClientTest() {
		Assert.assertEquals(controller.updateClient(cliente, ID).getStatusCode(), HttpStatus.CREATED);
	}
	
	@Test
	public void deleteClientTest() {
		when(clienteSvc.deleteClient(ID)).thenReturn(null);
		Assert.assertEquals(controller.deleteClient(ID).getStatusCode(), HttpStatus.OK);
	}
	
	
	
}